import 'package:flutter/material.dart';
import 'package:iconly/iconly.dart';
 void  main(List<String> args) {
  runApp(MyApp());
 }
 class MyApp extends StatelessWidget {
   const MyApp({ Key? key }) : super(key: key);
 
   @override
   Widget build(BuildContext context) {
     return MaterialApp(
       debugShowCheckedModeBanner: false,//untuk menghilangkan tulisan debug
       
       home: Scaffold(
         backgroundColor: Color.fromARGB(255, 19, 56, 87),
         body: Column(children: [
           SizedBox(height: 60),
           Center(child: Text('Belajar Container Widget', style: TextStyle(fontSize: 30, fontWeight: FontWeight.bold, color: Colors.white),)),
           SizedBox(height: 10),//memberikan space ruangkosong
           Text('By Noga Muktiwati (E41200415) A', style: TextStyle(fontSize: 15, color: Colors.white),),
           Image.asset('assets/images/noga.jpg',  height: 500, width: 500,),
           SizedBox(height: 20),
           Container(
             width: 180,
             height: 55,
             decoration: BoxDecoration(
               color: Color.fromARGB(255, 241, 246, 247),
               borderRadius: BorderRadius.circular(35)
             ),
             child: Row(
               children: [
                 SizedBox(width: 20),
                 Icon(IconlyBold.heart, size: 50, color:Color.fromARGB(255, 148, 7, 7)),
                 SizedBox(width: 10),
                 Text('Allah', style: TextStyle(fontSize: 20, color: Colors.blue, fontWeight: FontWeight.bold),),
               ],)
           )
          ],),
       ),
     );
   }
}